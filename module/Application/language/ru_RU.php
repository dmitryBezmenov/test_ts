<?php

return [
    'layout.navbar.task' => 'Задание',
    'layout.navbar.users' => 'Пользователи',
    'layout.navbar.places' => 'Места',
    'layout.navbar.departments' => 'Отделы',

    'index.title' => 'Тестовое задание',
    'index.jumbotron.title' => 'Это тестовое задание для Top Selection',
    'index.jumbotron.source_code' => 'Исходный код',

    'users.title' => 'Пользователи',
    'users.add' => 'Добавить',
    'users.table.fio' => 'ФИО',
    'users.table.age' => 'Возраст',
    'users.table.departments' => 'Отделы',
    'users.table.age_unknown' => 'неизвестно',
    'users.table.no_departments' => 'нет',

    'users.edit.title_start' => 'Пользователь',
    'users.edit.save' => 'Сохранить',
    'users.edit.delete' => 'Удалить',
    'users.edit.delete.confirm' => 'Вы уверены?',
    'users.edit.last_name' => 'Фамилия',
    'users.edit.first_name' => 'Имя',
    'users.edit.middle_name' => 'Отчество',
    'users.edit.birth_date' => 'Дата рождения',
    'users.edit.birth_place' => 'Место рождения',
    'users.edit.departments' => 'Отделы',

    'users.create.title' => 'Создать пользователя',
    'users.create.save' => 'Сохранить',
    'users.create.last_name' => 'Фамилия',
    'users.create.first_name' => 'Имя',
    'users.create.middle_name' => 'Отчество',
    'users.create.birth_date' => 'Дата рождения',
    'users.create.birth_place' => 'Место рождения',
    'users.create.departments' => 'Отделы',

    'places.title' => 'Места',
    'places.table.address' => 'Адрес',

    'departments.title' => 'Отделы',
    'departments.table.name' => 'Название',

    'user-edit.place-select.empty_option' => 'Выберите место рождения',
];
