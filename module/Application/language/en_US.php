<?php

return [
    'layout.navbar.task' => 'Task',
    'layout.navbar.users' => 'Users',
    'layout.navbar.places' => 'Places',
    'layout.navbar.departments' => 'Departments',

    'index.title' => 'Test task',
    'index.jumbotron.title' => 'This is a test task for Top Selection',
    'index.jumbotron.source_code' => 'Source code',

    'users.title' => 'Users',
    'users.add' => 'Add',
    'users.table.fio' => 'Full name',
    'users.table.age' => 'Age',
    'users.table.departments' => 'Departments',
    'users.table.age_unknown' => 'unknown',
    'users.table.no_departments' => 'no',

    'users.edit.title_start' => 'User',
    'users.edit.save' => 'Save',
    'users.edit.delete' => 'Delete',
    'users.edit.delete.confirm' => 'Are you sure?',
    'users.edit.last_name' => 'Last name',
    'users.edit.first_name' => 'First name',
    'users.edit.middle_name' => 'Middle name',
    'users.edit.birth_date' => 'Birth date',
    'users.edit.birth_place' => 'Birth place',
    'users.edit.departments' => 'Departments',

    'users.create.title' => 'Create new user',
    'users.create.save' => 'Save',
    'users.create.last_name' => 'Last name',
    'users.create.first_name' => 'First name',
    'users.create.middle_name' => 'Middle name',
    'users.create.birth_date' => 'Birth date',
    'users.create.birth_place' => 'Birth place',
    'users.create.departments' => 'Departments',

    'places.title' => 'Places',
    'places.table.address' => 'Address',

    'departments.title' => 'Departments',
    'departments.table.name' => 'Name',

    'user-edit.place-select.empty_option' => 'Select a birth place',
];
