<?php

declare(strict_types=1);

namespace Application;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Laminas\ServiceManager\Factory\InvokableFactory;
use Laminas\ServiceManager\AbstractFactory\ReflectionBasedAbstractFactory;
use Application\Controller\Factory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'users' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/users',
                    'defaults' => [
                        'controller' => Controller\UsersController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'edit' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => '/[:id]/edit',
                            'defaults' => [
                                'controller' => Controller\UsersController::class,
                                'action'     => 'edit',
                            ],
                            'constraints' => array(
                                'id' => '[0-9]+'
                            ),
                        ],
                    ],
                    'delete' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => '/[:id]/delete',
                            'defaults' => [
                                'controller' => Controller\UsersController::class,
                                'action'     => 'delete',
                            ],
                        ],
                    ],
                    'create' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => '/create',
                            'defaults' => [
                                'controller' => Controller\UsersController::class,
                                'action'     => 'create',
                            ],
                        ],
                    ],
                ],
            ],
            'places' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/places',
                    'defaults' => [
                        'controller' => Controller\PlacesController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'departments' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/departments',
                    'defaults' => [
                        'controller' => Controller\DepartmentsController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'locale' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/locale',
                    'defaults' => [
                        'controller' => Controller\LocaleController::class,
                        'action'     => 'switch',
                    ]
                ]
            ]
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\UsersController::class => Factory\UsersControllerFactory::class,
            Controller\PlacesController::class => Factory\PlacesControllerFactory::class,
            Controller\DepartmentsController::class => Factory\DepartmentsControllerFactory::class,
            Controller\LocaleController::class => ReflectionBasedAbstractFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            'application_driver' => [
                'class' => \Doctrine\ORM\Mapping\Driver\AttributeDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity',
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    'Application\Entity' => 'application_driver',
                ],
            ],
        ],
    ],
    'form_elements' => [
        'factories' => [
            \Application\Form\UserEdit\PlaceSelect::class => \Application\Form\UserEdit\Factory\PlaceSelectFactory::class,
            \Application\Form\UserEdit\DepartmentsMultiCheckbox::class => \Application\Form\UserEdit\Factory\DepartmentsMultiCheckboxFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            \Application\Form\UserEdit\Strategy\BirthPlaceStrategy::class => \Application\Form\UserEdit\Strategy\Factory\BirthPlaceStrategyFactory::class,
            \Application\Form\UserEdit\Strategy\DepartmentsStrategy::class => \Application\Form\UserEdit\Strategy\Factory\DepartmentsStrategyFactory::class,
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
