<?php

declare(strict_types=1);

namespace Application\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Application\Repository\PlaceRepository;
use Laminas\Paginator\Adapter;
use Laminas\Paginator\Paginator;

class PlacesController extends AbstractActionController
{
    public function __construct(
        private PlaceRepository $placeRepository,
    ) {}

    public function indexAction()
    {
        $places = $this->placeRepository->findAll();

        $paginator = new Paginator(new Adapter\ArrayAdapter($places));
        $paginator->setCurrentPageNumber($this->params()->fromQuery('page', 1));
        
        return new ViewModel([
            'places' => $paginator,
        ]);
    }
}
