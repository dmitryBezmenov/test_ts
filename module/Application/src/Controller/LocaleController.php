<?php

declare(strict_types=1);

namespace Application\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\Http\Request;
use Laminas\Http\Header\Referer;

class LocaleController extends AbstractActionController
{
    public function __construct(
        private Container $session,
    ) {}

    public function switchAction()
    {
        /** @var object $this->session */
        $this->session->locale = [
            'ru_RU' => 'en_US', 
            'en_US' => 'ru_RU'
        ][$this->session->locale ?? 'ru_RU'];

        /** @var Request $request */
        $request = $this->request;

        /** @var Referer $referer */
        $referer = $request->getHeader('Referer');

        return $this->redirect()->toUrl($referer?->getUri() ?? $this->url('index'));
    }
}
