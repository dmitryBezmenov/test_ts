<?php

declare(strict_types=1);

namespace Application\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Application\Repository\DepartmentRepository;
use Laminas\Paginator\Adapter;
use Laminas\Paginator\Paginator;

class DepartmentsController extends AbstractActionController
{
    public function __construct(
        private DepartmentRepository $departmentRepository,
    ) {}

    public function indexAction()
    {
        $departments = $this->departmentRepository->findAll();

        $paginator = new Paginator(new Adapter\ArrayAdapter($departments));
        $paginator->setCurrentPageNumber($this->params()->fromQuery('page', 1));
        
        return new ViewModel([
            'departments' => $paginator,
        ]);
    }
}
