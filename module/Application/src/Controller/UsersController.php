<?php

declare(strict_types=1);

namespace Application\Controller;

use Application\Entity\User;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Application\Form\UserEdit;
use Application\Repository\UserRepository;
use Laminas\Paginator\Adapter;
use Laminas\Paginator\Paginator;

class UsersController extends AbstractActionController
{
    public function __construct(
        private UserRepository $userRepository,
        private UserEdit $userEditForm,
    ) {}

    public function indexAction()
    {
        $users = $this->userRepository->findAll();

        $paginator = new Paginator(new Adapter\ArrayAdapter($users));
        $paginator->setCurrentPageNumber($this->params()->fromQuery('page', 1));
        
        return new ViewModel([
            'users' => $paginator,
        ]);
    }

    public function editAction()
    {
        $user = $this->userRepository->find($this->params()->fromRoute('id'));

        if (!$user) {
            $this->redirect()->toRoute('users');
            return;
        }

        $this->userEditForm->bind($user);

        /** @var \Laminas\Http\Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $this->userEditForm->setData($request->getPost());

            if ($this->userEditForm->isValid()) {
                $this->userRepository->save($user);
            }
        }
        
        return new ViewModel([
            'user' => $user,
            'form' => $this->userEditForm,
        ]);
    }

    public function deleteAction()
    {
        $user = $this->userRepository->find($this->params()->fromRoute('id'));

        if ($user) {
            $this->userRepository->delete($user);
        }

        $this->redirect()->toRoute('users');
    }

    public function createAction()
    {
        $user = new User();

        $this->userEditForm->bind($user);

        /** @var \Laminas\Http\Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $this->userEditForm->setData($request->getPost());

            if ($this->userEditForm->isValid()) {
                $this->userRepository->save($user);
                $this->redirect()->toRoute('users');
                return;
            }
        }
        
        return new ViewModel([
            'user' => $user,
            'form' => $this->userEditForm,
        ]);
    }
}
