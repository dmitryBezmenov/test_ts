<?php

namespace Application\Controller\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use Application\Controller\DepartmentsController;
use Doctrine\ORM\EntityManager;
use Application\Entity\Department;

class DepartmentsControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $em = $container->get(EntityManager::class);
        
        $departmentRepository = $em->getRepository(Department::class);

        return new DepartmentsController(
            $departmentRepository,
        );
    }
}
