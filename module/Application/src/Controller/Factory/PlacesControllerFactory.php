<?php

namespace Application\Controller\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use Application\Controller\PlacesController;
use Doctrine\ORM\EntityManager;
use Application\Entity\Place;

class PlacesControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $em = $container->get(EntityManager::class);
        
        $placeRepository = $em->getRepository(Place::class);

        return new PlacesController(
            $placeRepository,
        );
    }
}
