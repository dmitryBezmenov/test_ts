<?php

namespace Application\Controller\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use Application\Controller\UsersController;
use Doctrine\ORM\EntityManager;
use Application\Entity\User;
use Application\Form\UserEdit;
use Laminas\Form\FormElementManager;

class UsersControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $em = $container->get(EntityManager::class);
        
        $userRepository = $em->getRepository(User::class);
        $userEditForm = $container->get(FormElementManager::class)->get(UserEdit::class, [
            'container' => $container,
        ]);

        return new UsersController(
            $userRepository,
            $userEditForm,
        );
    }
}
