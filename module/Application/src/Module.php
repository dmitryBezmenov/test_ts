<?php

declare(strict_types=1);

namespace Application;

use Laminas\ModuleManager\Feature\InitProviderInterface;
use Laminas\ModuleManager\ModuleManagerInterface;
use Laminas\Mvc\MvcEvent;
use Laminas\Mvc\Application;
use Locale;
use Laminas\Session\Container;

class Module implements InitProviderInterface
{
    public function getConfig(): array
    {
        /** @var array $config */
        $config = include __DIR__ . '/../config/module.config.php';
        return $config;
    }

    public function init(ModuleManagerInterface $manager): void
    {
        $manager->getEventManager()->getSharedManager()->attach(
            Application::class,
            MvcEvent::EVENT_DISPATCH,
            function (MvcEvent $event) {
                $locale = $event->getApplication()->getServiceManager()->get(Container::class)->locale ?? 'ru_RU';
                $event->getViewModel()->setVariable('locale', $locale);
                Locale::setDefault($locale);
            }
        );
    }
}
