<?php

namespace Application\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Application\Repository;
use Application\Entity\Trait\HasId;

#[ORM\Entity(repositoryClass: Repository\DepartmentRepository::class)]
#[ORM\Table(name: 'departments')]
class Department
{
    use HasId;

    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    private int|null $id = null;

    #[ORM\Column(type: 'string', nullable: true, unique: true)]
    private string $name;

    /**
     * Сотрудники
     */
    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'departments')]
    private Collection $users;

    public function __construct() 
    {
        $this->users = new Collection();
    }

    public function setName(string $value): void
    {
        $this->name = $value;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }
}
