<?php

namespace Application\Entity\Value\User;

use Application\Entity\User;
use DateInterval;
use DateTime;

class Age
{
    public function __construct(
        public DateTime $birthDate
    ) {}

    public static function fromUser(User $user): ?static
    {
        if (is_null($birthDate = $user->getBirthDate())) {
            return null;
        }

        return new static($birthDate);
    }

    public function toString(): string
    {
        $years = $this->getDiff()->y;
        $plural = $years%10==1&&$years%100!=11?'год':($years%10>=2&&$years%10<=4&&($years%100<10||$years%100>=20)?'года':'лет');

        return "$years $plural";
    }

    public function getDiff(): DateInterval
    {
        return $this->birthDate->diff(new DateTime());
    }
}
