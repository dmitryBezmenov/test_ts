<?php

namespace Application\Entity\Value\User;

use Application\Entity\User;

class Fio
{
    public function __construct(
        public string $lastName,
        public string $firstName,
        public string $middleName,
    ) {}

    public static function fromUser(User $user): static
    {
        return new static(
            (string)$user->getLastName(),
            (string)$user->getFirstName(),
            (string)$user->getMiddleName(),
        );
    }

    public function toString(): string
    {
        return join(' ', array_filter([
            $this->lastName, $this->firstName, $this->middleName
        ], fn($str) => !empty($str)));
    }
}
