<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Repository;
use Application\Entity\Trait\HasId;

#[ORM\Entity(repositoryClass: Repository\PlaceRepository::class)]
#[ORM\Table(name: 'places')]
class Place
{
    use HasId;

    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    private int|null $id = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $address;

    public function setAddress(string $value): void
    {
        $this->address = $value;
    }

    public function getAddress(): string
    {
        return $this->address;
    }
}
