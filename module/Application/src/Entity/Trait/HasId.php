<?php

namespace Application\Entity\Trait;

trait HasId
{
    public function getId(): ?int
    {
        return $this->id;
    }
}
