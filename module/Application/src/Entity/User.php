<?php

namespace Application\Entity;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Application\Repository;
use Application\Entity\Value\User\Age;
use Application\Entity\Value\User\Fio;
use Application\Entity\Trait\HasId;

#[ORM\Entity(repositoryClass: Repository\UserRepository::class)]
#[ORM\Table(name: 'users')]
class User
{
    use HasId;

    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    private int|null $id = null;

    /**
     * Имя
     */
    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $firstName = null;

    /**
     * Фамилия
     */
    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $lastName = null;

    /**
     * Отчество
     */
    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $middleName = null;

    /**
     * Дата рождения
     */
    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?DateTime $birthDate = null;

    /**
     * Место рождения
     */
    #[ORM\ManyToOne(targetEntity: Place::class)]
    #[ORM\JoinColumn(name: 'place_id', referencedColumnName: 'id')]
    private ?Place $birthPlace = null;

    /**
     * Отделы
     */
    #[ORM\ManyToMany(targetEntity: Department::class, inversedBy: 'users')]
    #[ORM\JoinTable(name: 'users_departments')]
    private ?Collection $departments = null;

    public function setFirstName(string $value): void
    {
        $this->firstName = $value;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setLastName(string $value): void
    {
        $this->lastName = $value;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setMiddleName(string $value): void
    {
        $this->middleName = $value;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function setBirthDate(DateTime $value): void
    {
        $this->birthDate = $value;
    }

    public function getBirthDate(): ?DateTime
    {
        return $this->birthDate;
    }

    public function setBirthPlace(?Place $value): void
    {
        $this->birthPlace = $value;
    }

    public function getBirthPlace(): ?Place
    {
        return $this->birthPlace;
    }

    public function setDepartments(?Collection $departments): void
    {
        $this->departments = $departments;
    }

    public function getDepartments(): ?Collection
    {
        return $this->departments;
    }

    public function getFio(): Fio
    {
        return Fio::fromUser($this);
    }

    public function getAge(): ?Age
    {
        return Age::fromUser($this);
    }
}
