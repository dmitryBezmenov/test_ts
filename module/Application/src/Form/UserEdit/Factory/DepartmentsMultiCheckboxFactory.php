<?php

namespace Application\Form\UserEdit\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Application\Form\UserEdit\DepartmentsMultiCheckbox;
use Application\Entity\Department;

class DepartmentsMultiCheckboxFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, array $options = null)
    {
        $em = $container->get(EntityManager::class);
        $departmentRepository = $em->getRepository(Department::class);

        $departments = $departmentRepository->findAll();
        $options = array_map(fn($department) => [
            'label' => $department->getName(),
            'value' => $department->getId(),
        ], $departments);
        
        $placeSelect = new DepartmentsMultiCheckbox();
        $placeSelect->setValueOptions($options);

        return $placeSelect;
    }
}
