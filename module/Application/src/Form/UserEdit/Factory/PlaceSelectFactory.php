<?php

namespace Application\Form\UserEdit\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Application\Form\UserEdit\PlaceSelect;
use Application\Entity\Place;
use Laminas\I18n\Translator\TranslatorInterface;

class PlaceSelectFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, array $options = null)
    {
        $em = $container->get(EntityManager::class);
        $placesRepository = $em->getRepository(Place::class);

        $places = $placesRepository->findAll();
        $options = [];

        foreach ($places as $place) {
            $options[$place->getId()] = $place->getAddress();
        }

        $translator = $container->get(TranslatorInterface::class);

        $placeSelect = new PlaceSelect();
        $placeSelect->setOptions([
            // 'empty_option' => $translator->translate('user-edit.place-select.empty_option'),
            'value_options' => $options,
        ]);

        return $placeSelect;
    }
}
