<?php

namespace Application\Form\UserEdit\Strategy\Factory;

use Application\Entity\Department;
use Application\Form\UserEdit\Strategy\DepartmentsStrategy;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;

class DepartmentsStrategyFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $em = $container->get(EntityManager::class);
        
        $departmentRepository = $em->getRepository(Department::class);

        return new DepartmentsStrategy($departmentRepository);
    }
}
