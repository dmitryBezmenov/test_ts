<?php

namespace Application\Form\UserEdit\Strategy\Factory;

use Application\Entity\Place;
use Application\Form\UserEdit\Strategy\BirthPlaceStrategy;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;

class BirthPlaceStrategyFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $em = $container->get(EntityManager::class);
        
        $placeRepository = $em->getRepository(Place::class);

        return new BirthPlaceStrategy($placeRepository);
    }
}
