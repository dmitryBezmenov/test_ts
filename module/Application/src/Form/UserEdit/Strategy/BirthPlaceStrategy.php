<?php

namespace Application\Form\UserEdit\Strategy;

use Laminas\Hydrator\Strategy\StrategyInterface;
use Application\Entity\Place;
use Application\Repository\PlaceRepository;

class BirthPlaceStrategy implements StrategyInterface
{
    public function __construct(
        private PlaceRepository $placeRepository,
    ) {}

    public function extract($value, ?object $object = null)
    {
        /** @var Place $value */
        return $value?->getId();
    }

    public function hydrate($value, ?array $data)
    {
        if (!($id = intval($value))) {
            return $value;
        }

        return $this->placeRepository->find($id) ?? $value;
    }
}
