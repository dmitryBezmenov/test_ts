<?php

namespace Application\Form\UserEdit\Strategy;

use Laminas\Hydrator\Strategy\StrategyInterface;
use DateTime;

class BirthDateStrategy implements StrategyInterface
{
    public function extract($value, ?object $object = null)
    {
        return $value;
    }

    public function hydrate($value, ?array $data)
    {
        return DateTime::createFromFormat("Y-m-d", $value);
    }
}
