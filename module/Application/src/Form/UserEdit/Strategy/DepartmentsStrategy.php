<?php

namespace Application\Form\UserEdit\Strategy;

use Laminas\Hydrator\Strategy\StrategyInterface;
use Application\Repository\DepartmentRepository;
use Doctrine\Common\Collections\ArrayCollection;

class DepartmentsStrategy implements StrategyInterface
{
    public function __construct(
        private DepartmentRepository $departmentRepository,
    ) {}

    public function extract($value, ?object $object = null)
    {
        return $value?->map(fn($item) => $item->getId())->toArray();
    }

    public function hydrate($value, ?array $data)
    {
        if (!is_array($value) || empty($value)) {
            return new ArrayCollection();
        }

        $val = $this->departmentRepository->findBy(['id' => $value]);
        $val = new ArrayCollection($val);
        return $val;
    }
}
