<?php

namespace Application\Form;

use Application\Form\UserEdit\DepartmentsMultiCheckbox;
use Application\Form\UserEdit\PlaceSelect;
use Application\Form\UserEdit\Strategy\BirthDateStrategy;
use Application\Form\UserEdit\Strategy\BirthPlaceStrategy;
use Application\Form\UserEdit\Strategy\DepartmentsStrategy;
use Laminas\Form\Form;
use Laminas\Form\Element\Text;
use Laminas\Hydrator\ClassMethodsHydrator;
use Laminas\Form\Element\Date;
use Psr\Container\ContainerInterface;
use Laminas\Filter\StringTrim;
use Laminas\Validator\StringLength;

class UserEdit extends Form
{
    public function __construct(string $name = null, array $options = [])
    {
        /** @var ContainerInterface $container */
        if (!key_exists('container', $options) || !(($container = $options['container']) instanceof ContainerInterface)) {
            throw new \Exception('UserEdit form requires container in options array');
        }

        parent::__construct('user-edit');

        $hydrator = new ClassMethodsHydrator();
        $hydrator->addStrategy('birth_date', new BirthDateStrategy());
        $hydrator->addStrategy('birth_place', $container->get(BirthPlaceStrategy::class));
        $hydrator->addStrategy('departments', $container->get(DepartmentsStrategy::class));

        $this->setHydrator($hydrator);
        $this->setAttribute('method', 'post');
    }

    public function init()
    {
        $this->add([
            'name' => 'last_name',
            'type' => Text::class,
        ]);

        $this->getInputFilter()->add([
            'name' => 'last_name',
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 128,
                    ]
                ]
            ]
        ]);

        $this->add([
            'name' => 'first_name',
            'type' => Text::class,
        ]);

        $this->getInputFilter()->add([
            'name' => 'first_name',
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 128,
                    ]
                ]
            ]
        ]);

        $this->add([
            'name' => 'middle_name',
            'type' => Text::class,
        ]);

        $this->add([
            'name' => 'birth_date',
            'type' => Date::class,
        ]);

        $this->add([
            'name' => 'birth_place',
            'type' => PlaceSelect::class,
        ]);

        $this->add([
            'name' => 'departments',
            'type' => DepartmentsMultiCheckbox::class,
        ]);
    }
}
