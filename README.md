Для работы требуются:

1) бд mysql

2) указать в `config/autoload/local.php` данные для подключения к бд

3) запустить миграции (`./vendor/bin/doctrine-module migrations:migrate`)

4) загрузить начальные данные (`source data/database/fixtures/init.sql`)

схема бд в `data/database/fixtures/schema.sql`
